# Protobuf编解码

参考 [Protobuf编解码](https://gitee.com/infraboard/go-course/blob/master/day15/protobuf.md#https://gitee.com/link?target=https%3A%2F%2Fgithub.com%2Fprotocolbuffers%2Fprotobuf%2Freleases)

## IDE(Vscode)

+ vscode-proto3

如果是windows: import "google/protobuf/any.proto" , 存放路径: /usr/local/include
```json
{
    "protoc": {
        "path": "/path/to/protoc",
        "compile_on_save": false,
        "options": [
            "--proto_path=<google 规范目录, incloude>",
        ]
    }
}
```

## 编译

```sh
# hello world 目录
protoc -I=. --go_out=. hello.proto 
# gitlab.com/go-course-project/go13/skills/protobuf/hello
# gitlab.com/go-course-project/go13/skills/
protoc -I=. --go_out=. --go_opt=module="gitlab.com/go-course-project/go13/skills/protobuf/hello" hello.proto

# go mod 的项目目录(标准) 在skill 目录下main
protoc -I=. --go_out=. --go_opt=module="gitlab.com/go-course-project/go13/skills" protobuf/hello/hello.proto  
```

+ -I：-IPATH, --proto_path=PATH, 指定proto文件搜索的路径, 如果有多个路径 可以多次使用-I 来指定, 如果不指定默认为当前目录
+ --go_out: --go指插件的名称, 我们安装的插件为: protoc-gen-go, 而protoc-gen是插件命名规范, go是插件名称, 因此这里是--go, 而--go_out 表示的是 go插件的 out参数, 这里指编译产物的存放目录
+ --go_opt: protoc-gen-go插件opt参数, 这里的module指定了go module, 生成的go pkg 会去除掉module路径，生成对应pkg
pb/hello.proto: 我们proto文件路径


```proto3
message QueryBlogRequest {
    // 分页大小, 一个多少个
    // page_size -> PageSize
    int64 page_size = 1;
    // 当前页, 查询哪一页的数据
    int64 page_number = 2;
    // 谁创建的文章
    string create_by = 3;
    // 通过文字名字进行关键字搜索
    string keywords = 4;
}
```

```go
type QueryBlogRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// 分页大小, 一个多少个
	// page_size -> PageSize
	PageSize int64 `protobuf:"varint,1,opt,name=page_size,json=pageSize,proto3" json:"page_size,omitempty"`
	// 当前页, 查询哪一页的数据
	PageNumber int64 `protobuf:"varint,2,opt,name=page_number,json=pageNumber,proto3" json:"page_number,omitempty"`
	// 谁创建的文章
	CreateBy string `protobuf:"bytes,3,opt,name=create_by,json=createBy,proto3" json:"create_by,omitempty"`
	// 通过文字名字进行关键字搜索
	Keywords string `protobuf:"bytes,4,opt,name=keywords,proto3" json:"keywords,omitempty"`
}
```


```proto3
enum STATUS {
    // 草稿
    DRAFT = 0;
    // 已发布
    PUBLISHED = 1;
}
```


```go
type STATUS int32

const (
	// 草稿
	STATUS_DRAFT STATUS = 0
	// 已发布
	STATUS_PUBLISHED STATUS = 1
)

// Enum value maps for STATUS.
var (
	STATUS_name = map[int32]string{
		0: "DRAFT",
		1: "PUBLISHED",
	}
	STATUS_value = map[string]int32{
		"DRAFT":     0,
		"PUBLISHED": 1,
	}
)
```

```proto3
message QueryBlogRequestSet {
    // 多个请求 items : []QueryBlogRequest
    repeated QueryBlogRequest items = 1;
}
```

```go
type QueryBlogRequestSet struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// 多个请求 items : []QueryBlogRequest
	Items []*QueryBlogRequest `protobuf:"bytes,1,rep,name=items,proto3" json:"items,omitempty"`
}
```


```proto3
message Sub1 {
    string name = 1;
}

message Sub2 {
    string name = 1;
}

message SampleMessage {
    oneof test_oneof {
        Sub1 sub1 = 1;
        Sub2 sub2 = 2;
    }
}
```

```go
// Oneof
type Sub1 struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Name string `protobuf:"bytes,1,opt,name=name,proto3" json:"name,omitempty"`
}

type Sub2 struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Name string `protobuf:"bytes,1,opt,name=name,proto3" json:"name,omitempty"`
}

type SampleMessage struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// Types that are assignable to TestOneof:
	//
	//	*SampleMessage_Sub1
	//	*SampleMessage_Sub2
	TestOneof isSampleMessage_TestOneof `protobuf_oneof:"test_oneof"`
}

type isSampleMessage_TestOneof interface {
	isSampleMessage_TestOneof()
}

type SampleMessage_Sub1 struct {
	Sub1 *Sub1 `protobuf:"bytes,1,opt,name=sub1,proto3,oneof"`
}

type SampleMessage_Sub2 struct {
	Sub2 *Sub2 `protobuf:"bytes,2,opt,name=sub2,proto3,oneof"`
}

func (*SampleMessage_Sub1) isSampleMessage_TestOneof() {}

func (*SampleMessage_Sub2) isSampleMessage_TestOneof() {}

func (x *SampleMessage) GetSub1() *Sub1 {
	if x, ok := x.GetTestOneof().(*SampleMessage_Sub1); ok {
		return x.Sub1
	}
	return nil
}

func (x *SampleMessage) GetSub2() *Sub2 {
	if x, ok := x.GetTestOneof().(*SampleMessage_Sub2); ok {
		return x.Sub2
	}
	return nil
}
```


### Any

```proto3
// Any
message ErrorStatus {
    string message = 1;
    repeated google.protobuf.Any details = 2;
}
```

```go
// Any
type ErrorStatus struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Message string       `protobuf:"bytes,1,opt,name=message,proto3" json:"message,omitempty"`
	Details []*anypb.Any `protobuf:"bytes,2,rep,name=details,proto3" json:"details,omitempty"`
}
```

``` sh
# go mod 的项目目录(标准) 在skill 目录下main
# 文件统配
protoc -I=. --go_out=. --go_opt=module="gitlab.com/go-course-project/go13/skills" protobuf/*/*.proto 
```