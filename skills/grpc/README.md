# GRPC

protobuf 就是是定义业务接口(描述业务)

``` sh
# go mod 的项目目录(标准) 在skill 目录下main
# 文件统配
protoc -I=. --go_out=. --go-grpc_out=. --go-grpc_opt=module="gitlab.com/go-course-project/go13/skills" --go_opt=module="gitlab.com/go-course-project/go13/skills" grpc/server/pb/*.proto 
```

## 服务端

业务接口约束:
```go
// HelloServiceServer is the server API for HelloService service.
// All implementations must embed UnimplementedHelloServiceServer
// for forward compatibility
type HelloServiceServer interface {
	Greet(context.Context, *GreetRequest) (*GreetResponse, error)
	mustEmbedUnimplementedHelloServiceServer()
}
```

```go
type HelloServiceServerImpl struct {
    // 继承基础的方法
    *pb.UnimplementedHelloServiceServer
}

// 覆盖掉他的方法, 编写真正的业务逻辑
func (i *HelloServiceServerImpl) Greet(context.Context, *GreetRequest) (*GreetResponse, error) {
    return nil, nil
}

func init() {
    // 注册业务实现到grpc服务
    RegisterHelloServiceServer(gprcServer, &HelloServiceServerImpl{})
}

// 首先是通过grpc.NewServer()构造一个gRPC服务对象
server := grpc.NewServer()
pb.RegisterHelloServiceServer(server, &HelloServiceServerImpl{})

lis, err := net.Listen("tcp", ":1234")
if err != nil {
    log.Fatal(err)
}
err = server.Serve(lis)
if err != nil {
    log.Fatal(err)
}
```

## 客户端

自动生成

```go
func main() {
	// 需要先建立网络链接
	WithInsecure := grpc.WithTransportCredentials(insecure.NewCredentials())
	conn, err := grpc.Dial("localhost:1234", WithInsecure)
	if err != nil {
		panic(err)
	}

	client := pb.NewHelloServiceClient(conn)
	resp, err := client.Greet(context.Background(), &pb.GreetRequest{
		Name: "bob",
	})
	if err != nil {
		panic(err)
	}
	log.Println(resp)
}
```

