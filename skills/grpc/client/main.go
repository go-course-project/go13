package main

import (
	"context"
	"log"

	"gitlab.com/go-course-project/go13/skills/grpc/server/pb"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

// PerRPCCredentials defines the common interface for the credentials which need to
// attach security information to every RPC (e.g., oauth2).
// type PerRPCCredentials interface {
// 	// GetRequestMetadata gets the current request metadata, refreshing tokens
// 	// if required. This should be called by the transport layer on each
// 	// request, and the data should be populated in headers or other
// 	// context. If a status code is returned, it will be used as the status for
// 	// the RPC (restricted to an allowable set of codes as defined by gRFC
// 	// A54). uri is the URI of the entry point for the request.  When supported
// 	// by the underlying implementation, ctx can be used for timeout and
// 	// cancellation. Additionally, RequestInfo data will be available via ctx
// 	// to this call.  TODO(zhaoq): Define the set of the qualified keys instead
// 	// of leaving it as an arbitrary string.
// 	GetRequestMetadata(ctx context.Context, uri ...string) (map[string]string, error)
// 	// RequireTransportSecurity indicates whether the credentials requires
// 	// transport security.
// 	RequireTransportSecurity() bool
// }

// 用PerRPCCredentials 携带凭证

func NewClientAuthentication(clientId, clientSecret string) *Authentication {
	return &Authentication{
		clientID:     clientId,
		clientSecret: clientSecret,
	}
}

// Authentication todo
// Authentication 来实现 PerRPCCredentials, 负责携带凭证
type Authentication struct {
	clientID     string
	clientSecret string
}

// GetRequestMetadata todo
func (a *Authentication) GetRequestMetadata(context.Context, ...string) (
	map[string]string, error,
) {
	return map[string]string{
		"client-id":     a.clientID,
		"client-secret": a.clientSecret,
	}, nil
}

// RequireTransportSecurity todo
func (a *Authentication) RequireTransportSecurity() bool {
	return false
}

func main() {
	// 需要先建立网络链接
	WithInsecure := grpc.WithTransportCredentials(insecure.NewCredentials())
	authOpt := grpc.WithPerRPCCredentials(NewClientAuthentication(
		"admin",
		"123456",
	))
	conn, err := grpc.Dial("localhost:1234", WithInsecure, authOpt)
	if err != nil {
		panic(err)
	}

	// 需要携带认证信息
	// cient-id/client-secret

	client := pb.NewHelloServiceClient(conn)
	resp, err := client.Greet(context.Background(), &pb.GreetRequest{
		Name: "bob",
	})
	if err != nil {
		panic(err)
	}
	log.Println(resp)
}
