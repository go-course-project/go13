package main

import (
	"context"
	"fmt"
	"log"
	"net"

	"gitlab.com/go-course-project/go13/skills/grpc/server/middlewares"
	"gitlab.com/go-course-project/go13/skills/grpc/server/pb"
	"google.golang.org/grpc"
)

// 编写业务具体实现
type HelloServiceServerImpl struct {
	// 继承基础的方法
	*pb.UnimplementedHelloServiceServer
}

// 覆盖掉他的方法, 编写真正的业务逻辑
func (i *HelloServiceServerImpl) Greet(
	ctx context.Context,
	in *pb.GreetRequest,
) (*pb.GreetResponse, error) {
	return &pb.GreetResponse{
		Message: fmt.Sprintf("hello, %s", in.Name),
	}, nil
}

func main() {
	// 首先是通过grpc.NewServer()构造一个gRPC服务对象
	// Req/Resp
	// WithUnaryInterceptor --> Req/Resp 模式下的中间件，r.Use()
	// opt1 := grpc.UnaryInterceptor(middeware1)
	// opt2 := grpc.UnaryInterceptor(middeware2)
	// Stream
	// opt1 := grpc.StreamInterceptor(middeware1)
	// opt2 := grpc.StreamInterceptor(middeware2)
	// grpc.NewServer(opt1 opt2)
	// opt1 := grpc.UnaryInterceptor(middeware1)

	// 补充认证中间件
	opt1 := grpc.UnaryInterceptor(middlewares.GrpcAuthUnaryServerInterceptor())
	server := grpc.NewServer(opt1)

	pb.RegisterHelloServiceServer(server, &HelloServiceServerImpl{})

	lis, err := net.Listen("tcp", ":1234")
	if err != nil {
		log.Fatal(err)
	}
	err = server.Serve(lis)
	if err != nil {
		log.Fatal(err)
	}
}
