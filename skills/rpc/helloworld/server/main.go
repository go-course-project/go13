package main

import (
	"fmt"
	"io"
	"net/http"
	"net/rpc"
	"net/rpc/jsonrpc"

	"gitlab.com/go-course-project/go13/skills/rpc/helloworld/service"
)

func main() {
	// 把我们的对象注册成一个rpc的 receiver
	// 其中rpc.Register函数调用会将对象类型中所有满足RPC规则的对象方法注册为RPC函数，
	// 所有注册的方法会放在“HelloService”服务空间之下
	// HelloService{}.Greet()  ---> HelloService.Greet
	rpc.RegisterName("HelloService", new(HelloService))

	// 然后我们建立一个唯一的TCP链接，
	// listener, err := net.Listen("tcp", ":1234")
	// if err != nil {
	// 	log.Fatal("ListenTCP error:", err)
	// }

	// for {
	// 	conn, err := listener.Accept()
	// 	if err != nil {
	// 		log.Fatal("Accept error:", err)
	// 	}

	// 	// 代码中最大的变化是用rpc.ServeCodec函数替代了rpc.ServeConn函数，
	// 	// 传入的参数是针对服务端的json编解码器
	// 	go rpc.ServeCodec(jsonrpc.NewServerCodec(conn))
	// }

	// RPC的服务架设在“/jsonrpc”路径, rpc --> http handler
	http.HandleFunc("/jsonrpc", func(w http.ResponseWriter, r *http.Request) {
		// rpc 框架来负责响应  http request/response
		// request/response ---> rpc 能够读取编解码器
		// request/response ---> io.ReadWriteCloser
		// type ReadWriteCloser interface {
		// 	Reader
		// 	Writer
		// 	Closer
		// }
		conn := NewRPCReadWriteCloserFromHTTP(w, r)
		rpc.ServeRequest(jsonrpc.NewServerCodec(conn))
	})

	http.ListenAndServe(":1234", nil)
}

func NewRPCReadWriteCloserFromHTTP(w http.ResponseWriter, r *http.Request) *RPCReadWriteCloser {
	return &RPCReadWriteCloser{w, r.Body}
}

type RPCReadWriteCloser struct {
	io.Writer
	io.ReadCloser
}

// 实现接口的服务实例
var _ service.HelloService = (*HelloService)(nil)

// rpc:   127.0.0.1:1234.Greet('my name bob') ---> hello, bob
// HelloService.Greet() ---> rpc
// 写好的复合rpc框架的业务模块, 注册给rpc
type HelloService struct{}

// 这个方法通过rpc对我暴露: net/rpc
// 需要编写符合 net/rpc 框架 暴露标准的 函数
// rpc ---> fn  rpc:  (request any, response any) error
func (s *HelloService) Greet(req string, resp *string) error {
	*resp = fmt.Sprintf("hello, %s", req)
	return nil
}
