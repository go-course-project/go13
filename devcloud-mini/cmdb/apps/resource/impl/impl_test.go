package impl_test

import (
	"context"
	"fmt"
	"os"

	"github.com/infraboard/mcube/v2/ioc"
	"gitlab.com/go-course-project/go13/devcloud-mini/cmdb/apps/resource"

	// 加载所有模块
	_ "gitlab.com/go-course-project/go13/devcloud-mini/cmdb/apps"
)

var (
	impl resource.Service
	ctx  = context.Background()
)

func init() {
	os.Setenv("MONGO_DATABASE", "cmdb")
	ioc.DevelopmentSetup()

	fmt.Println(ioc.Controller().List())
	impl = ioc.Controller().Get(resource.AppName).(resource.Service)
}
