package apps

import (
	_ "gitlab.com/go-course-project/go13/devcloud-mini/cmdb/apps/resource/api"
	_ "gitlab.com/go-course-project/go13/devcloud-mini/cmdb/apps/resource/impl"
	_ "gitlab.com/go-course-project/go13/devcloud-mini/cmdb/apps/secret/api"
	_ "gitlab.com/go-course-project/go13/devcloud-mini/cmdb/apps/secret/impl"
)
