package provider

import (
	"context"

	"gitlab.com/go-course-project/go13/devcloud-mini/cmdb/apps/resource"
)

type ResourceSyncConfig struct {
	ApiKey    string
	ApiSecret string
	Region    string
}

type SyncResourceHandler func(context.Context, *resource.Resource)

type ResourceProvider interface {
	Sync(ctx context.Context, hanleFunc SyncResourceHandler) error
}
