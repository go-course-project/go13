package apps

import (
	_ "gitlab.com/go-course-project/go13/devcloud-mini/maudit/apps/audit/consumer"
	_ "gitlab.com/go-course-project/go13/devcloud-mini/maudit/apps/audit/exporter"
	_ "gitlab.com/go-course-project/go13/devcloud-mini/maudit/apps/audit/impl"
)
