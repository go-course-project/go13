# 流水线

![](./image.png)


## Job 执行

kaniko 封装命令 或者镜像进行封装, [官方地址](https://github.com/GoogleContainerTools/kaniko#pushing-to-docker-hub)
![alt text](image-1.png)

封装 K8s Job: 
```yml
apiVersion: batch/v1 # 版本号
kind: Job # 类型
metadata: # 元数据
  name: test-job # job 名称
  namespace: default # 所属命名空间
  labels: #标签
    app: app01
spec: # 详情描述
  completions: 1 # 指定 job 需要成功运行 Pods 的次数。默认值: 1
  parallelism: 1 # 指定 job 在任一时刻应该并发运行 Pods 的数量。默认值: 1，如果上面的 completions 为 6 ，这个参数为 3 ，表示有 6 个 pod，允许有 3 个 pod 并发运行
  activeDeadlineSeconds: 900 # 指定 job 可运行的时间期限，超过时间还未结束，系统将会尝试进行终止。
  backoffLimit: 1 # 指定 job 失败后进行重试的次数。默认是 6
  # manualSelector: true # 是否可以使用 selector 选择器选择 pod，默认是 false
  # selector: # 选择器，通过它指定该控制器管理哪些 pod
  #   matchLabels:      # Labels 匹配规则
  #     app: counter-pod
  # matchExpressions: # Expressions 匹配规则
  #   - {key: app, operator: In, values: [counter-pod]}
  template: # 模板，当副本数量不足时，会根据下面的模板创建 pod 副本
    metadata:
      labels:
        app: build-pod
    spec:
      restartPolicy: Never # 重启策略只能设置为 Never 或者 OnFailure
      initContainers:
        - name: download
          image: registry.cn-hangzhou.aliyuncs.com/godev/git:2.39.2
          workingDir: /
          imagePullPolicy: IfNotPresent
          command: ["sh"]
          args:
            [
              "-c",
              "git clone ${GIT_SSH_URL} workspace --branch=${GIT_BRANCH} --single-branch && cd workspace && git reset --hard ${GIT_COMMIT_ID}",
            ]
          env:
            - name: GIT_SSH_COMMAND
              value: ssh -i /etc/secret-volume/id_rsa -o StrictHostKeyChecking=no
          volumeMounts:
            - name: workspace
              mountPath: /workspace
            - name: secret-volume
              readOnly: true
              mountPath: "/etc/secret-volume"
      containers:
        - image: gcr.lank8s.cn/kaniko-project/executor:v1.22.0-debug
          workingDir: /workspace
          imagePullPolicy: IfNotPresent
          name: build-and-push
          command: ["sh"]
          # /workspace/runtime/task.env 文件是用收集输出的环境变量的
          args:
            [
              "-c",
              "/kaniko/executor --cache=${CACHE_ENABLE} --cache-repo=${CACHE_REPO} --compressed-caching=${CACHE_COMPRESS} --custom-platform=${CUSTOM_PLATFORM} --destination=${IMAGE_REPOSITORY}:${APP_VERSION} --dockerfile=${APP_DOCKERFILE}",
            ]
          volumeMounts:
            - name: workspace
              mountPath: /workspace
            - name: kaniko-secret
              mountPath: /kaniko/.docker
      volumes:
        - name: workspace
          emptyDir: {}
        - name: secret-volume
          secret:
            secretName: ${git_ssh_secret}
            defaultMode: 0600
        - name: kaniko-secret
          secret:
            secretName: ${image_push_secret}
```

Job 执行逻辑
```go
func (i *impl) RunJob(ctx context.Context, in *pipeline.Task) (
	*task.JobTask, error) {
	ins := task.NewJobTask(in)

	// 获取之前任务的状态, 因为里面有当前任务的审核状态
	err := i.GetJotTaskStatus(ctx, ins)
	if err != nil {
		return nil, err
	}

	// 开启审核后, 执行任务则 调整审核状态为等待中
	if ins.Spec.Audit.Enable {
		auditStatus := ins.AuditStatus()
		switch auditStatus {
		case pipeline.AUDIT_STAGE_PENDDING:
			ins.AuditStatusFlowTo(pipeline.AUDIT_STAGE_WAITING)
		case pipeline.AUDIT_STAGE_DENY:
			return nil, fmt.Errorf("任务审核未通过")
		}
		i.log.Debug().Msgf("任务: %s 审核状态为, %s", ins.Spec.TaskId, auditStatus)
	}

	// 如果不忽略执行, 并且审核通过, 则执行
	if in.Enabled() && ins.AuditPass() {
		// 任务状态检查与处理
		switch ins.Status.Stage {
		case task.STAGE_PENDDING:
			ins.Status.Stage = task.STAGE_CREATING
		case task.STAGE_ACTIVE:
			return nil, exception.NewConflict("任务: %s 当前处于运行中, 需要等待运行结束后才能执行", in.TaskId)
		}

		// 查询需要执行的Job
		req := job.NewDescribeJobRequestByName(in.JobName)

		j, err := i.job.DescribeJob(ctx, req)
		if err != nil {
			return nil, err
		}
		ins.Job = j
		ins.Spec.JobId = j.Meta.Id
		i.log.Info().Msgf("describe job success, %s[%s]", j.Spec.Name, j.Meta.Id)

		// 脱敏参数动态还原
		in.RunParams.RestoreSensitive(j.Spec.RunParams)

		// 合并允许参数(Job里面有默认值), 并检查参数合法性
		// 注意Param的合并是有顺序的，也就是参数优先级(低-->高):
		// 1. 系统变量(默认禁止修改)
		// 2. job默认变量
		// 3. job运行变量
		// 4. pipeline 运行变量
		// 5. pipeline 运行时变量
		params := job.NewRunParamSet()
		params.Add(ins.SystemRunParam()...)
		params.Add(j.Spec.RunParams.Params...)
		params.Merge(in.RunParams.Params...)
		err = i.LoadPipelineRunParam(ctx, in, params)
		if err != nil {
			return nil, err
		}

		// 校验参数合法性
		err = params.Validate()
		if err != nil {
			return nil, fmt.Errorf("校验任务【%s】参数错误, %s", j.Spec.DisplayName, err)
		}
		i.log.Info().Msgf("params check ok, %s", params)

		// 获取执行器执行
		r := runner.GetRunner(j.Spec.RunnerType)
		runReq := task.NewRunTaskRequest(ins.Spec.TaskId, j.Spec.RunnerSpec, params)
		runReq.DryRun = in.RunParams.DryRun
		runReq.Labels = in.Labels
		runReq.ManualUpdateStatus = j.Spec.ManualUpdateStatus

		i.log.Debug().Msgf("[%s] start run task: %s", ins.Spec.PipelineTask, in.TaskName)
		status, err := r.Run(ctx, runReq)
		if err != nil {
			return nil, fmt.Errorf("run job error, %s", err)
		}
		status.RunParams = params
		ins.Status = status

		// 添加搜索标签
		ins.BuildSearchLabel()
	}

	// 保存任务
	updateOpt := options.Update()
	updateOpt.SetUpsert(true)
	if _, err := i.jcol.UpdateByID(ctx, ins.Spec.TaskId, bson.M{"$set": ins}, updateOpt); err != nil {
		return nil, exception.NewInternalServerError("upsert a job task document error, %s", err)
	}
	return ins, nil
}
```

渲染完成后，调用k8s client 执行的job
``` yml
apiVersion: batch/v1
kind: Job
metadata:
  annotations:
    task.mflow.inforboar.io/id: task-coqelqh97i67olecsor0
  creationTimestamp: "2024-05-03T13:47:22Z"
  generation: 1
  labels:
    app: app01
  name: task-coqelqh97i67olecsor0
  namespace: default
  resourceVersion: "2164966"
  uid: 4960f322-353b-4fe7-8e9a-c5e8b0dfe9e2
spec:
  activeDeadlineSeconds: 900
  backoffLimit: 1
  completionMode: NonIndexed
  completions: 1
  manualSelector: false
  parallelism: 1
  podReplacementPolicy: TerminatingOrFailed
  selector:
    matchLabels:
      batch.kubernetes.io/controller-uid: 4960f322-353b-4fe7-8e9a-c5e8b0dfe9e2
  suspend: false
  template:
    metadata:
      creationTimestamp: null
      labels:
        app: build-pod
        batch.kubernetes.io/controller-uid: 4960f322-353b-4fe7-8e9a-c5e8b0dfe9e2
        batch.kubernetes.io/job-name: task-coqelqh97i67olecsor0
        controller-uid: 4960f322-353b-4fe7-8e9a-c5e8b0dfe9e2
        job-name: task-coqelqh97i67olecsor0
    spec:
      containers:
      - args:
        - -c
        - /kaniko/executor --cache=${CACHE_ENABLE} --cache-repo=${CACHE_REPO} --compressed-caching=${CACHE_COMPRESS}
          --custom-platform=${CUSTOM_PLATFORM} --destination=${IMAGE_REPOSITORY}:${APP_VERSION}
          --dockerfile=${APP_DOCKERFILE}
        command:
        - sh
        env:
        - name: JOB_TASK_ID
          value: task-coqelqh97i67olecsor0
        - name: JOB_TASK_UPDATE_TOKEN
          value: coqelqh97i67olecsorg
        - name: JOB_ID
          value: co16q180iubgbnpmb12g
        - name: PIPELINE_TASK_ID
          value: coqelqh97i67olecsoqg
        - name: GIT_SSH_URL
          value: git@gitlab.com:infraboard/cicd_test.git
        - name: GIT_BRANCH
          value: main
        - name: GIT_COMMIT_ID
          value: ea3193053ac9c9eaa8a7fc89468cfc6524d570c0
        - name: CACHE_ENABLE
          value: "true"
        - name: CACHE_REPO
          value: registry.cn-hangzhou.aliyuncs.com/build_cache/cicd_test
        - name: CACHE_COMPRESS
          value: "true"
        - name: CUSTOM_PLATFORM
          value: linux/amd64
        - name: IMAGE_REPOSITORY
          value: registry.cn-hangzhou.aliyuncs.com/infraboard/cicd_test
        - name: APP_VERSION
          value: v20240503-main-ea319305
        - name: APP_DOCKERFILE
          value: Dockerfile
        image: gcr.lank8s.cn/kaniko-project/executor:v1.22.0-debug
        imagePullPolicy: IfNotPresent
        name: build-and-push
        resources: {}
        terminationMessagePath: /dev/termination-log
        terminationMessagePolicy: File
        volumeMounts:
        - mountPath: /workspace
          name: workspace
        - mountPath: /kaniko/.docker
          name: kaniko-secret
        workingDir: /workspace
      dnsPolicy: ClusterFirst
      initContainers:
      - args:
        - -c
        - git clone ${GIT_SSH_URL} workspace --branch=${GIT_BRANCH} --single-branch
          && cd workspace && git reset --hard ${GIT_COMMIT_ID}
        command:
        - sh
        env:
        - name: GIT_SSH_COMMAND
          value: ssh -i /etc/secret-volume/id_rsa -o StrictHostKeyChecking=no
        - name: JOB_TASK_ID
          value: task-coqelqh97i67olecsor0
        - name: JOB_TASK_UPDATE_TOKEN
          value: coqelqh97i67olecsorg
        - name: JOB_ID
          value: co16q180iubgbnpmb12g
        - name: PIPELINE_TASK_ID
          value: coqelqh97i67olecsoqg
        - name: GIT_SSH_URL
          value: git@gitlab.com:infraboard/cicd_test.git
        - name: GIT_BRANCH
          value: main
        - name: GIT_COMMIT_ID
          value: ea3193053ac9c9eaa8a7fc89468cfc6524d570c0
        - name: CACHE_ENABLE
          value: "true"
        - name: CACHE_REPO
          value: registry.cn-hangzhou.aliyuncs.com/build_cache/cicd_test
        - name: CACHE_COMPRESS
          value: "true"
        - name: CUSTOM_PLATFORM
          value: linux/amd64
        - name: IMAGE_REPOSITORY
          value: registry.cn-hangzhou.aliyuncs.com/infraboard/cicd_test
        - name: APP_VERSION
          value: v20240503-main-ea319305
        - name: APP_DOCKERFILE
          value: Dockerfile
        image: registry.cn-hangzhou.aliyuncs.com/godev/git:2.39.2
        imagePullPolicy: IfNotPresent
        name: download
        resources: {}
        terminationMessagePath: /dev/termination-log
        terminationMessagePolicy: File
        volumeMounts:
        - mountPath: /workspace
          name: workspace
        - mountPath: /etc/secret-volume
          name: secret-volume
          readOnly: true
        workingDir: /
      restartPolicy: Never
      schedulerName: default-scheduler
      securityContext: {}
      terminationGracePeriodSeconds: 30
      volumes:
      - emptyDir: {}
        name: workspace
      - name: secret-volume
        secret:
          defaultMode: 384
          secretName: git-ssh-key
      - name: kaniko-secret
        secret:
          defaultMode: 420
          secretName: kaniko-secret
status:
  completionTime: "2024-05-03T13:56:20Z"
  conditions:
  - lastProbeTime: "2024-05-03T13:56:20Z"
    lastTransitionTime: "2024-05-03T13:56:20Z"
    status: "True"
    type: Complete
  ready: 0
  startTime: "2024-05-03T13:47:22Z"
  succeeded: 1
  terminating: 0
  uncountedTerminatedPods: {}
```

## 流水线 执行(手动运行)

### Pipeline定义

![alt text](image-2.png)


### 运行Pipeline

```go
func (h *PipelineTaskHandler) RunPipeline(r *restful.Request, w *restful.Response) {
	req := pipeline.NewRunPipelineRequest("")
	if err := r.ReadEntity(req); err != nil {
		response.Failed(w, err)
		return
	}
	req.UpdateFromToken(token.GetTokenFromRequest(r))
	set, err := h.service.RunPipeline(r.Request.Context(), req)
	if err != nil {
		response.Failed(w, err)
		return
	}
	response.Success(w, set)
}
```

```go
// 执行Pipeline
func (i *impl) RunPipeline(ctx context.Context, in *pipeline.RunPipelineRequest) (
	*task.PipelineTask, error) {
	// 检查Pipeline请求参数
	if err := in.Validate(); err != nil {
		return nil, exception.NewBadRequest(err.Error())
	}

	// 查询需要执行的Pipeline
	pReq := pipeline.NewDescribePipelineRequest(in.PipelineId)
	pReq.WithJob = true
	p, err := i.pipeline.DescribePipeline(ctx, pReq)
	if err != nil {
		return nil, err
	}

	// 检查Pipeline状态
	if err := i.CheckPipelineAllowRun(ctx, p, in.ApprovalId); err != nil {
		return nil, err
	}

	// 从pipeline 取出需要执行的任务
	ins := task.NewPipelineTask(p, in)
	ts, err := ins.NextRun()
	if err != nil {
		return nil, fmt.Errorf("find next run task error, %s", err)
	}
	if ts == nil || ts.Len() == 0 {
		return nil, fmt.Errorf("not job task to run")
	}
	if in.DryRun {
		return ins, nil
	}

	// 保存Job Task, 所有JobTask 批量生成, 全部处于Pendding状态, 然后入库 等待状态更新
	err = i.JobTaskBatchSave(ctx, ins.JobTasks())
	if err != nil {
		return nil, err
	}

	ins.MarkedRunning()
	// 保存Pipeline状态
	if _, err := i.pcol.InsertOne(ctx, ins); err != nil {
		return nil, exception.NewInternalServerError("inserted a pipeline task document error, %s", err)
	}

	/*
		运行 第一批Task, 驱动Pipeline执行
	*/
	i.log.Debug().Msgf("run pipeline tasks: %v", ts.TaskNames())
	for _, t := range ts.Items {
		uReq := task.NewUpdateJobTaskStatusRequest(t.Spec.TaskId)
		uReq.UpdateToken = t.Spec.UpdateToken
		reqp, err := i.RunJob(ctx, t.Spec)
		if err != nil {
			uReq.MarkError(err)
		} else {
			uReq.Stage = reqp.Status.Stage
		}
		_, err = i.UpdateJobTaskStatus(ctx, uReq)
		if err != nil {
			i.log.Error().Msgf("update pipeline status form task error, %s", err)
		}
	}
	return ins, nil
}
```


寻找下一个需要执行的任务:
```go
// 返回下个需要执行的JobTask, 允许一次并行执行多个(批量执行)
// Task DryRun属性要继承PipelineTask
func (p *PipelineTask) NextRun() (*JobTaskSet, error) {
	set := NewJobTaskSet()
	var stage *StageStatus

	if p.Status == nil || p.Pipeline == nil {
		return set, nil
	}

	// 需要未执行完成的Job Tasks
	stages := p.Status.StageStatus
	for i := range stages {
		stage = stages[i]

		// 找出Stage中未执行完的Job Task
		set = stage.UnCompleteJobTask(p.Params.DryRun)
		set.UpdateFromPipelineTask(p)
		// 如果找到 直接Break
		if set.Len() > 0 {
			break
		}
	}

	// 如果所有Stage寻找完，都没找到, 表示PipelineTask执行完成
	if set.Len() == 0 {
		return set, nil
	}

	// 如果这些未执行当中的Job Task 有处于运行中的, 不会执行下个一个任务
	if set.HasStage(STAGE_ACTIVE) {
		return set, exception.NewConflict("Stage 还处于运行中")
	}

	// 当未执行的任务中，没有运行中的时，剩下的就是需要被执行的任务
	tasks := set.GetJobTaskByStage(STAGE_PENDDING)

	nextTasks := NewJobTaskSet()
	stageSpec := p.Pipeline.GetStage(stage.Name)
	if stageSpec.IsParallel {
		// 并行任务 返回该Stage所有等待执行的job
		nextTasks.Add(tasks...)
	} else {
		// 串行任务取第一个
		nextTasks.Add(tasks[0])
	}

	return nextTasks.UpdateFromPipelineTask(p), nil
}
```

### Pipeline 状态驱动

利用operator 来触发 Pipiline对象状态更新

1. 更新Job Task状态
```go
// 更新Job状态
func (i *impl) UpdateJobTaskStatus(ctx context.Context, in *task.UpdateJobTaskStatusRequest) (
	*task.JobTask, error) {
	ins, err := i.DescribeJobTask(ctx, task.NewDescribeJobTaskRequest(in.Id))
	if err != nil {
		return nil, err
	}

	// 校验更新合法性
	err = i.CheckAllowUpdate(ctx, ins, in.UpdateToken, in.ForceUpdateStatus)
	if err != nil {
		return nil, err
	}

	i.log.Debug().Msgf("更新任务状态: %s，当前状态: %s, 更新状态: %s",
		ins.Spec.TaskId, ins.Status.Stage, in.Stage)
	// 状态更新
	ins.Status.UpdateStatus(in)

	// 更新数据库
	if err := i.updateJobTaskStatus(ctx, ins); err != nil {
		return nil, err
	}

	// Job Task状态变更回调
	i.JobTaskStatusChangedCallback(ctx, ins)

	// Pipeline Task 状态变更回调
	if ins.Spec.PipelineTask != "" {
		// 如果状态未变化, 不触发流水线更新
		if !in.ForceTriggerPipeline && !ins.Status.Changed {
			i.log.Debug().Msgf("task %s status not changed: [%s], skip update pipeline", in.Id, in.Stage)
			return ins, nil
		}
		_, err := i.PipelineTaskStatusChanged(ctx, ins)
		if err != nil {
			return nil, err
		}
	}
	return ins, nil
}
```

2. 更新Job Task关联的Pipeline状态

```go
// Pipeline中任务有变化时,
// 如果执行成功则 继续执行, 如果失败则标记Pipeline结束
// 当所有任务成功结束时标记Pipeline执行成功
func (i *impl) PipelineTaskStatusChanged(ctx context.Context, in *task.JobTask) (
	*task.PipelineTask, error) {
	if in == nil || in.Status == nil {
		return nil, exception.NewBadRequest("job task or job task status is nil")
	}

	if in.Spec.PipelineTask == "" {
		return nil, exception.NewBadRequest("Pipeline Id参数缺失")
	}

	runErrorJobTasks := []*task.UpdateJobTaskStatusRequest{}
	// 获取Pipeline Task, 因为Job Task是先保存在触发的回调, 这里获取的Pipeline Task是最新的
	descReq := task.NewDescribePipelineTaskRequest(in.Spec.PipelineTask)
	p, err := i.DescribePipelineTask(ctx, descReq)
	if err != nil {
		return nil, err
	}

	defer func() {
		// 更新当前任务的pipeline task状态
		i.mustUpdatePipelineStatus(ctx, p)

		// 如果JobTask正常执行, 则等待回调更新, 如果执行失败 则需要立即更新JobTask状态
		for index := range runErrorJobTasks {
			_, err = i.UpdateJobTaskStatus(ctx, runErrorJobTasks[index])
			if err != nil {
				p.MarkedFailed(err)
				i.mustUpdatePipelineStatus(ctx, p)
			}
		}
	}()

	// 更新Pipeline Task 运行时环境变量
	p.Status.RuntimeEnvs.Merge(in.RuntimeRunParams()...)

	switch in.Status.Stage {
	case task.STAGE_PENDDING,
		task.STAGE_SCHEDULING,
		task.STAGE_CREATING,
		task.STAGE_ACTIVE,
		task.STAGE_CANCELING:
		// Task状态无变化
		return p, nil
	case task.STAGE_CANCELED:
		// 任务取消, pipeline 取消执行
		p.MarkedCanceled()
		return p, nil
	case task.STAGE_FAILED:
		// 任务执行失败, 更新Pipeline状态为失败
		if !in.Spec.RunParams.IgnoreFailed {
			p.MarkedFailed(in.Status.MessageToError())
			return p, nil
		}
	case task.STAGE_SUCCEEDED:
		// 任务运行成功, pipeline继续执行
		i.log.Info().Msgf("task: %s run successed", in.Spec.TaskId)
	}

    // 3. Pipeline 再次出发 Job 执行
	/*
		task执行成功或者忽略执行失败, 此时pipeline 仍然处于运行中, 需要获取下一个任务执行
	*/
	nexts, err := p.NextRun()
	if err != nil {
		p.MarkedFailed(err)
		return p, nil
	}

	// 如果没有需要执行的任务, Pipeline执行结束, 更新Pipeline状态为成功
	if nexts == nil || nexts.Len() == 0 {
		p.MarkedSuccess()
		return p, nil
	}

	// 如果有需要执行的JobTask, 继续执行
	for index := range nexts.Items {
		item := nexts.Items[index]
		// 如果任务执行成功则等待任务的回调更新任务状态
		// 如果任务执行失败, 直接更新任务状态
		t, err := i.RunJob(ctx, item.Spec)
		if err != nil {
			updateT := task.NewUpdateJobTaskStatusRequest(item.Spec.TaskId)
			updateT.UpdateToken = item.Spec.UpdateToken
			updateT.MarkError(err)
			runErrorJobTasks = append(runErrorJobTasks, updateT)
		} else {
			item.Status = t.Status
			item.Job = t.Job
		}
	}

	return p, nil
}
```

## WebHook集成(自动触发)

###  关于WebHook(Gitlab)

关于官方说明: [Gitlba WebHook](https://docs.gitlab.com/ee/user/project/integrations/webhooks.html)

![alt text](image-3.png)

![alt text](image-4.png)

Push 事件
```json
{
  "object_kind": "push",
  "event_name": "push",
  "before": "95790bf891e76fee5e1747ab589903a6a1f80f22",
  "after": "da1560886d4f094c3e6c9ef40349f7d38b5d27d7",
  "ref": "refs/heads/master",
  "ref_protected": true,
  "checkout_sha": "da1560886d4f094c3e6c9ef40349f7d38b5d27d7",
  "user_id": 4,
  "user_name": "John Smith",
  "user_username": "jsmith",
  "user_email": "john@example.com",
  "user_avatar": "https://s.gravatar.com/avatar/d4c74594d841139328695756648b6bd6?s=8://s.gravatar.com/avatar/d4c74594d841139328695756648b6bd6?s=80",
  "project_id": 15,
  "project":{
    "id": 15,
    "name":"Diaspora",
    "description":"",
    "web_url":"http://example.com/mike/diaspora",
    "avatar_url":null,
    "git_ssh_url":"git@example.com:mike/diaspora.git",
    "git_http_url":"http://example.com/mike/diaspora.git",
    "namespace":"Mike",
    "visibility_level":0,
    "path_with_namespace":"mike/diaspora",
    "default_branch":"master",
    "homepage":"http://example.com/mike/diaspora",
    "url":"git@example.com:mike/diaspora.git",
    "ssh_url":"git@example.com:mike/diaspora.git",
    "http_url":"http://example.com/mike/diaspora.git"
  },
  "repository":{
    "name": "Diaspora",
    "url": "git@example.com:mike/diaspora.git",
    "description": "",
    "homepage": "http://example.com/mike/diaspora",
    "git_http_url":"http://example.com/mike/diaspora.git",
    "git_ssh_url":"git@example.com:mike/diaspora.git",
    "visibility_level":0
  },
  "commits": [
    {
      "id": "b6568db1bc1dcd7f8b4d5a946b0b91f9dacd7327",
      "message": "Update Catalan translation to e38cb41.\n\nSee https://gitlab.com/gitlab-org/gitlab for more information",
      "title": "Update Catalan translation to e38cb41.",
      "timestamp": "2011-12-12T14:27:31+02:00",
      "url": "http://example.com/mike/diaspora/commit/b6568db1bc1dcd7f8b4d5a946b0b91f9dacd7327",
      "author": {
        "name": "Jordi Mallach",
        "email": "jordi@softcatala.org"
      },
      "added": ["CHANGELOG"],
      "modified": ["app/controller/application.rb"],
      "removed": []
    },
    {
      "id": "da1560886d4f094c3e6c9ef40349f7d38b5d27d7",
      "message": "fixed readme",
      "title": "fixed readme",
      "timestamp": "2012-01-03T23:36:29+02:00",
      "url": "http://example.com/mike/diaspora/commit/da1560886d4f094c3e6c9ef40349f7d38b5d27d7",
      "author": {
        "name": "GitLab dev user",
        "email": "gitlabdev@dv6700.(none)"
      },
      "added": ["CHANGELOG"],
      "modified": ["app/controller/application.rb"],
      "removed": []
    }
  ],
  "total_commits_count": 4
}
```

写一个Trigger 来处理来自 外部系统的的WebHook事件


### Web Hook 接收

```go
// 处理来自gitlab的事件
// Hook Header参考文档: https://docs.gitlab.com/ee/user/project/integrations/webhooks.html#delivery-headers
// 参考文档: https://docs.gitlab.com/ee/user/project/integrations/webhook_events.html
func (h *Handler) HandleGitlabEvent(r *restful.Request, w *restful.Response) {
	event, err := trigger.ParseGitLabEventFromRequest(r)
	if err != nil {
		response.Failed(w, err)
		return
	}

	// 专门处理Gilab事件
	ge, err := event.GetGitlabEvent()
	if err != nil {
		response.Failed(w, err)
		return
	}
	event.SubName = ge.GetBranch()

	h.log.Debug().Msgf("accept event: %s", event.ToJson())
	ins, err := h.svc.HandleEvent(r.Request.Context(), event)
	if err != nil {
		response.Failed(w, err)
		return
	}
	response.Success(w, ins)
}
```

### 关于规则匹配

![](./webhook.drawio)

![alt text](image-5.png)

![alt text](image-6.png)

![alt text](image-7.png)

找到服务 定义的出发规范

![alt text](image-8.png)

### 处理事件

```go
// 应用事件处理
func (i *impl) HandleEvent(ctx context.Context, in *trigger.Event) (
	*trigger.Record, error) {
	if err := in.Validate(); err != nil {
		return nil, exception.NewBadRequest(err.Error())
	}

	ins := trigger.NewRecord(in)

	// 查询服务相关信息
	i.log.Debug().Msgf("查询事件关联服务信息...")
	svc, err := i.mcenter.Service().DescribeService(
		ctx,
		service.NewDescribeServiceRequest(in.Token),
	)
	if err != nil {
		return nil, exception.NewBadRequest("查询服务%s异常, %s", in.Token, err)
	}
	ins.Event.ServiceInfo = svc.Desense().ToJSON()

	// 获取该服务对应事件的触发配置
	i.log.Debug().Msgf("查询该服务是否有关联构建配置...")
	req := build.NewQueryBuildConfigRequest()
	req.AddService(in.Token)
	req.Event = in.Name
	req.SetEnabled(true)
	set, err := i.build.QueryBuildConfig(ctx, req)
	if err != nil {
		return nil, err
	}

	if set.Len() == 0 {
		return nil, fmt.Errorf("没有找到服务[%s]的构建规则", svc.Spec.Name)
	}

	// 子事件匹配, 找到事件分支的构建流程 触发规则: Push Hook/main
	i.log.Debug().Msgf("服务[%s]关联%d个构建配置", svc.Spec.Name, set.Len())
	matched := set.MatchSubEvent(in.SubName)
	for index := range matched.Items {
		// 执行构建配置匹配的流水线
		buildConf := matched.Items[index]
		bs := i.RunBuildConf(ctx, in, buildConf)
		ins.AddBuildStatus(bs)
	}

	// 保存
	if _, err := i.col.InsertOne(ctx, ins); err != nil {
		return nil, exception.NewInternalServerError("inserted a deploy document error, %s", err)
	}
	return ins, nil
}
```

### 触发构建配置

把事件的解析出来的参数，注入到流水线中运行

```go
func (i *impl) RunBuildConf(ctx context.Context, in *trigger.Event, buildConf *build.BuildConfig) *trigger.BuildStatus {
	bs := trigger.NewBuildStatus(buildConf)

	pipelineId := buildConf.Spec.PipelineId
	if pipelineId == "" {
		bs.ErrorMessage = "未配置流水线"
		return bs
	}

	runReq := pipeline.NewRunPipelineRequest(pipelineId)
	runReq.RunBy = "@" + in.UUID()
	runReq.TriggerMode = pipeline.TRIGGER_MODE_EVENT
	runReq.DryRun = in.SkipRunPipeline
	runReq.Labels[trigger.PIPELINE_TASK_EVENT_LABLE_KEY] = in.Id
	runReq.Labels[build.PIPELINE_TASK_BUILD_CONFIG_LABLE_KEY] = buildConf.Meta.Id
	runReq.Domain = buildConf.Spec.Scope.Domain
	runReq.Namespace = buildConf.Spec.Scope.Namespace

	// 补充Build用户自定义变量
	runReq.AddRunParam(buildConf.Spec.CustomParams...)

	// 补充Gitlab事件特有的变量
	switch in.Provider {
	case trigger.EVENT_PROVIDER_GITLAB:
		event, err := in.GetGitlabEvent()
		if err != nil {
			bs.ErrorMessage = err.Error()
			return bs
		}

		// 补充Git信息
		runReq.AddRunParam(in.GitRunParams().Params...)
		// 补充版本信息
		switch buildConf.Spec.VersionNamedRule {
		case build.VERSION_NAMED_RULE_DATE_BRANCH_COMMIT:
			runReq.AddRunParam(event.DateCommitVersion(buildConf.Spec.VersionPrefix))
		case build.VERSION_NAMED_RULE_GIT_TAG:
			runReq.AddRunParam(event.TagVersion(buildConf.Spec.VersionPrefix))
		}
	}

	i.log.Debug().Msgf("run pipeline req: %s, params: %v", runReq.PipelineId, runReq.RunParamsKVMap())
	pt, err := i.task.RunPipeline(ctx, runReq)
	if err != nil {
		i.log.Debug().Msgf("run pipeline error, %s", err)
		bs.ErrorMessage = err.Error()
	}
	if pt != nil {
		i.log.Debug().Msgf("update run build conf pipeline task id: %s", pt.Meta.Id)
		bs.PiplineTaskId = pt.Meta.Id
		bs.PiplineTask = pt
	}
	return bs
}
```

注入 GitLab相关的变量信息
```go
func (e *GitlabWebHookEvent) GitRunParams(params *job.RunParamSet) {
	// 补充项目相关信息
	params.Add(
		job.NewRunParam(VARIABLE_GIT_PROJECT_NAME, e.Project.Name).SetSearchLabel(true),
		job.NewRunParam(VARIABLE_GIT_SSH_URL, e.Project.GitSshUrl).SetSearchLabel(true),
		job.NewRunParam(VARIABLE_GIT_HTTP_URL, e.Project.GitHttpUrl).SetSearchLabel(true),
	)

	switch e.EventType {
	case GITLAB_EVENT_TYPE_PUSH:
		params.Add(
			job.NewRunParam(VARIABLE_GIT_BRANCH, e.GetBranch()),
		)
		cm := e.GetLatestCommit()
		if cm != nil {
			params.Add(job.NewRunParam(VARIABLE_GIT_COMMIT, cm.Id))
		}
	case GITLAB_EVENT_TYPE_TAG:
		params.Add(
			job.NewRunParam(VARIABLE_GIT_TAG, e.GetTag()),
		)
	case GITLAB_EVENT_TYPE_MERGE_REQUEST:
		oa := e.ObjectAttributes
		params.Add(
			job.NewRunParam(VARIABLE_GIT_MR_ACTION, oa.Action),
			job.NewRunParam(VARIABLE_GIT_MR_STATUS, oa.MergeStatus),
			job.NewRunParam(VARIABLE_GIT_MR_SOURCE_BRANCE, oa.SourceBranch),
			job.NewRunParam(VARIABLE_GIT_MR_TARGET_BRANCE, oa.TargetBranch),
		)
		if oa.LastCommit != nil {
			params.Add(job.NewRunParam(VARIABLE_GIT_COMMIT, oa.LastCommit.Id))
		}
	case GITLAB_EVENT_TYPE_COMMENT:
	case GITLAB_EVENT_TYPE_ISSUE:
	}
}
```