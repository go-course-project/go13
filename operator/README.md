# k8s operator 实战

[官方文档](https://book.kubebuilder.io/)


```sh
# download kubebuilder and install locally.
curl -L -o kubebuilder "https://go.kubebuilder.io/dl/latest/$(go env GOOS)/$(go env GOARCH)"
chmod +x kubebuilder && mv kubebuilder /usr/local/bin/
```

```sh
➜  go13 git:(main) ✗ kubebuilder version
Version: main.version{KubeBuilderVersion:"3.14.2", KubernetesVendor:"1.27.1", GitCommit:"d7b4febe6b673709100b780b5b99151c5e26a206", BuildDate:"2024-04-27T08:27:24Z", GoOs:"darwin", GoArch:"arm64"}
```

```sh
➜  go13 git:(main) ✗ kubebuilder -h
CLI tool for building Kubernetes extensions and tools.

Usage:
  kubebuilder [flags]
  kubebuilder [command]

Examples:
The first step is to initialize your project:
    kubebuilder init [--plugins=<PLUGIN KEYS> [--project-version=<PROJECT VERSION>]]

<PLUGIN KEYS> is a comma-separated list of plugin keys from the following table
and <PROJECT VERSION> a supported project version for these plugins.

                             Plugin keys | Supported project versions
-----------------------------------------+----------------------------
               base.go.kubebuilder.io/v3 |                          3
               base.go.kubebuilder.io/v4 |                          3
        declarative.go.kubebuilder.io/v1 |                       2, 3
 deploy-image.go.kubebuilder.io/v1-alpha |                          3
                    go.kubebuilder.io/v2 |                       2, 3
                    go.kubebuilder.io/v3 |                          3
                    go.kubebuilder.io/v4 |                          3
         grafana.kubebuilder.io/v1-alpha |                          3
      kustomize.common.kubebuilder.io/v1 |                          3
      kustomize.common.kubebuilder.io/v2 |                          3

For more specific help for the init command of a certain plugins and project version
configuration please run:
    kubebuilder init --help --plugins=<PLUGIN KEYS> [--project-version=<PROJECT VERSION>]

Default plugin keys: "go.kubebuilder.io/v4"
Default project version: "3"


Available Commands:
  alpha       Alpha-stage subcommands
  completion  Load completions for the specified shell
  create      Scaffold a Kubernetes API or webhook
  edit        Update the project configuration
  help        Help about any command
  init        Initialize a new project
  version     Print the kubebuilder version

Flags:
  -h, --help                     help for kubebuilder
      --plugins strings          plugin keys to be used for this subcommand execution
      --project-version string   project version (default "3")

Use "kubebuilder [command] --help" for more information about a command.
```

## 项目初始化

```sh
➜  go13 git:(main) ✗ kubebuilder init -h
Initialize a new project including the following files:
  - a "go.mod" with project dependencies
  - a "PROJECT" file that stores project configuration
  - a "Makefile" with several useful make targets for the project
  - several YAML files for project deployment under the "config" directory
  - a "cmd/main.go" file that creates the manager that will run the project controllers

Usage:
  kubebuilder init [flags]

Examples:
  # Initialize a new project with your domain and name in copyright
  kubebuilder init --plugins go/v4 --domain example.org --owner "Your name"

  # Initialize a new project defining a specific project version
  kubebuilder init --plugins go/v4 --project-version 3


Flags:
      --domain string            domain for groups (default "my.domain")
      --fetch-deps               ensure dependencies are downloaded (default true)
  -h, --help                     help for init
      --license string           license to use to boilerplate, may be one of 'apache2', 'none' (default "apache2")
      --owner string             owner to add to the copyright
      --project-name string      name of this project
      --project-version string   project version (default "3")
      --repo string              name to use for go module (e.g., github.com/user/repo), defaults to the go package of the current working directory.
      --skip-go-version-check    if specified, skip checking the Go version

Global Flags:
      --plugins strings   plugin keys to be used for this subcommand execution
```

```sh
➜  operator git:(main) ✗ kubebuilder init  --domain go13.org --owner "oldyu" --repo="gitlab.com/go-course-project/go13/operator"
INFO Writing kustomize manifests for you to edit... 
INFO Writing scaffold for you to edit...          
INFO Get controller runtime:
$ go get sigs.k8s.io/controller-runtime@v0.17.3 
go: downloading sigs.k8s.io/controller-runtime v0.17.3
go: downloading k8s.io/apimachinery v0.29.2
go: downloading k8s.io/client-go v0.29.2
go: downloading k8s.io/api v0.29.2
go: downloading github.com/evanphx/json-patch/v5 v5.8.0
go: downloading k8s.io/apiextensions-apiserver v0.29.2
go: downloading k8s.io/component-base v0.29.2
go: downloading github.com/imdario/mergo v0.3.6
go: downloading github.com/google/uuid v1.3.0
go: downloading golang.org/x/oauth2 v0.12.0
INFO Update dependencies:
$ go mod tidy           
go: downloading github.com/onsi/ginkgo/v2 v2.14.0
go: downloading github.com/onsi/gomega v1.30.0
Next: define a resource with:
$ kubebuilder create api
```

```sh
➜  operator git:(main) ✗ make help

Usage:
  make <target>

General
  help             Display this help.

Development
  manifests        Generate WebhookConfiguration, ClusterRole and CustomResourceDefinition objects.
  generate         Generate code containing DeepCopy, DeepCopyInto, and DeepCopyObject method implementations.
  fmt              Run go fmt against code.
  vet              Run go vet against code.
  test             Run tests.
  lint             Run golangci-lint linter & yamllint
  lint-fix         Run golangci-lint linter and perform fixes

Build
  build            Build manager binary.
  run              Run a controller from your host.
  docker-build     Build docker image with the manager.
  docker-push      Push docker image with the manager.
  docker-buildx    Build and push docker image for the manager for cross-platform support
  build-installer  Generate a consolidated YAML with CRDs and deployment.

Deployment
  install          Install CRDs into the K8s cluster specified in ~/.kube/config.
  uninstall        Uninstall CRDs from the K8s cluster specified in ~/.kube/config. Call with ignore-not-found=true to ignore resource not found errors during deletion.
  deploy           Deploy controller to the K8s cluster specified in ~/.kube/config.
  undeploy         Undeploy controller from the K8s cluster specified in ~/.kube/config. Call with ignore-not-found=true to ignore resource not found errors during deletion.

Dependencies
  kustomize        Download kustomize locally if necessary.
  controller-gen   Download controller-gen locally if necessary.
  envtest          Download setup-envtest locally if necessary.
  golangci-lint    Download golangci-lint locally if necessary.
```


## CRD开发

查询当前k8s集群里面的crd定义
```sh
➜  operator git:(main) ✗ kubectl get crd
```

```
$ kubebuilder create api
```

### 定义业务

定义业务API是一样
```
CRD: (Restful API Stuct)
service_name:  cmdb(k8s service, cluster ip, client-go) 
domain:    api.cmdb.go13.com
```

```sh
➜  operator git:(main) ✗ kubectl api-resources;
NAME                              SHORTNAMES   APIVERSION                        NAMESPACED   KIND
bindings                                       v1                                true         Binding
componentstatuses                 cs           v1                                false        ComponentStatus
```


使用 create api 来创建 struct
```sh
➜  operator git:(main) ✗ kubebuilder create api -h
Scaffold a Kubernetes API by writing a Resource definition and/or a Controller.

If information about whether the resource and controller should be scaffolded
was not explicitly provided, it will prompt the user if they should be.

After the scaffold is written, the dependencies will be updated and
make generate will be run.

Usage:
  kubebuilder create api [flags]

Examples:
  # Create a frigates API with Group: ship, Version: v1beta1 and Kind: Frigate
  kubebuilder create api --group ship --version v1beta1 --kind Frigate

  # Edit the API Scheme

  nano api/v1beta1/frigate_types.go

  # Edit the Controller
  nano internal/controller/frigate/frigate_controller.go

  # Edit the Controller Test
  nano internal/controller/frigate/frigate_controller_test.go

  # Generate the manifests
  make manifests

  # Install CRDs into the Kubernetes cluster using kubectl apply
  make install

  # Regenerate code and run against the Kubernetes cluster configured by ~/.kube/config
  make run


Flags:
      --controller           if set, generate the controller without prompting the user (default true)
      --force                attempt to create resource even if it already exists
      --group string         resource Group
  -h, --help                 help for api
      --kind string          resource Kind
      --make make generate   if true, run make generate after generating files (default true)
      --namespaced           resource is namespaced (default true)
      --plural string        resource irregular plural form
      --resource             if set, generate the resource without prompting the user (default true)
      --version string       resource Version

Global Flags:
      --plugins strings   plugin keys to be used for this subcommand execution
```


### 初始化CRD

```sh
# go work use ./operator 
# 已经存在 --force
 kubebuilder create api --group devops --version v1beta1 --kind DnsRecord
 INFO Create Resource [y/n]                        
y
INFO Create Controller [y/n]                      
y
INFO Writing kustomize manifests for you to edit... 
INFO Writing scaffold for you to edit...          
INFO api/v1beta1/dnsrecord_types.go               
INFO api/v1beta1/groupversion_info.go             
INFO internal/controller/suite_test.go            
INFO internal/controller/dnsrecord_controller.go  
INFO internal/controller/dnsrecord_controller_test.go 
INFO Update dependencies:
$ go mod tidy           
INFO Running make:
$ make generate                
/Users/yumaojun/Projects/go-course/go13/operator/bin/controller-gen-v0.14.0 object:headerFile="hack/boilerplate.go.txt" paths="./..."
Next: implement your new API and generate the manifests (e.g. CRDs,CRs) with:
$ make manifests
```

API对象都在api目录下: dnsrecord_types.go

```sh
//+kubebuilder:object:root=true
//+kubebuilder:subresource:status

// DnsRecord is the Schema for the dnsrecords API
type DnsRecord struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   DnsRecordSpec   `json:"spec,omitempty"`
	Status DnsRecordStatus `json:"status,omitempty"`
}
```

### 编辑业务CRD

定义自己的API
```sh
// DnsRecordSpec defines the desired state of DnsRecord
type DnsRecordSpec struct {
	// INSERT ADDITIONAL SPEC FIELDS - desired state of cluster
	// Important: Run "make" to regenerate code after modifying this file

	// Foo is an example field of DnsRecord. Edit dnsrecord_types.go to remove/update
	// Foo string `json:"foo,omitempty"`

	Service string `json:"service"`
	Domain  string `json:"domain"`
}

// DnsRecordStatus defines the observed state of DnsRecord
type DnsRecordStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "make" to regenerate code after modifying this file
	Stage   STAGE  `json:"stage"`
	Message string `json:"message"`
}

type STAGE int8

const (
	STAGE_WAITING = iota
	STAGE_SUCCESS
	STAGE_FAILED
)
```

### 安装CRD

```sh
  # Generate the manifests
  # devops.go13.org_dnsrecords.yaml
  make manifests


  # 生成字段变更后新代码
   make generate

  # Install CRDs into the Kubernetes cluster using kubectl apply
  make install
```

```sh
➜  operator git:(main) ✗ kubectl get crd
NAME                         CREATED AT
dnsrecords.devops.go13.org   2024-05-05T03:08:55Z
```

### 测试CRD

```yaml
apiVersion: devops.go13.org/v1beta1
kind: DnsRecord
metadata:
  labels:
    app.kubernetes.io/name: operator
    app.kubernetes.io/managed-by: kustomize
  name: dnsrecord-sample
spec:
  # TODO(user): Add fields here
  service: 'cmdb'
  domain: 'api.comdb.go13.com'
```

```sh
➜  operator git:(main) ✗ kubectl apply -f config/samples/devops_v1beta1_dnsrecord.yaml 
dnsrecord.devops.go13.org/dnsrecord-sample created
```

```sh
➜  operator git:(main) ✗ kubectl api-resources | grep dns
dnsrecords                                     devops.go13.org/v1beta1           true         DnsRecord
```

```sh
➜  operator git:(main) ✗ kubectl get DnsRecord dnsrecord-sample -o yaml
apiVersion: devops.go13.org/v1beta1
kind: DnsRecord
metadata:
  annotations:
    kubectl.kubernetes.io/last-applied-configuration: |
      {"apiVersion":"devops.go13.org/v1beta1","kind":"DnsRecord","metadata":{"annotations":{},"labels":{"app.kubernetes.io/managed-by":"kustomize","app.kubernetes.io/name":"operator"},"name":"dnsrecord-sample","namespace":"default"},"spec":{"domain":"api.comdb.go13.com","service":"cmdb"}}
  creationTimestamp: "2024-05-05T03:13:02Z"
  generation: 1
  labels:
    app.kubernetes.io/managed-by: kustomize
    app.kubernetes.io/name: operator
  name: dnsrecord-sample
  namespace: default
  resourceVersion: "2229828"
  uid: 3ba1ab2b-9711-4914-8650-c5ddff71ecf9
spec:
  domain: api.comdb.go13.com
  service: cmdb
```

```sh
➜  operator git:(main) ✗ kubectl delete DnsRecord dnsrecord-sample                    
dnsrecord.devops.go13.org "dnsrecord-sample" deleted
```

## Controller

internal/controller: 所有的controler
```sh
➜  controller git:(main) ✗ ls   
dnsrecord_controller.go      dnsrecord_controller_test.go suite_test.go
```

controller生成代码:
```go
// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
// TODO(user): Modify the Reconcile function to compare the state specified by
// the DnsRecord object against the actual cluster state, and then
// perform operations to make the cluster state reflect the state specified by
// the user.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.17.3/pkg/reconcile
func (r *DnsRecordReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	_ = log.FromContext(ctx)

	// TODO(user): your logic here
  // 1. 获取对象


  // 2. 判断对象状态


  // 3. 达成期望

	return ctrl.Result{}, nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *DnsRecordReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&devopsv1beta1.DnsRecord{}).
		Complete(r)
}

```


### 编写业务逻辑

```go
func (r *DnsRecordReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	l := log.FromContext(ctx)

	// TODO(user): your logic here
	// 1. 获取对象, json Unmarshal
	obj := &devopsv1beta1.DnsRecord{}
	if err := r.Get(ctx, req.NamespacedName, obj); err != nil {
		if apierrors.IsNotFound(err) {
			// 执行删除逻辑, {module: "dnsrecord"}
			l.Info("执行删除逻辑", "module", "dnsrecord")
		}
		return ctrl.Result{}, client.IgnoreNotFound(err)
	}

	l.Info("获取对象成功", "service", obj.Spec.Service, "domain", obj.Spec.Domain)

	// 2. 判断对象状态
	if obj.Status.Stage > devopsv1beta1.STAGE_WAITING {
		l.Info("已经解析")
		return ctrl.Result{}, nil
	}

	// 3. 达成期望
	l.Info("调用DNS来进行解析")
	// 更加具体的执行结果修改状态
	obj.Status.Stage = devopsv1beta1.STAGE_SUCCESS

	// 更新状态, 通过update方法来更新对象
	if err := r.Update(ctx, obj); err != nil {
		l.Error(err, "update dns record error")
	}

	l.Info("调用DNS来进行解析成功")
	return ctrl.Result{}, nil
}
```

### 测试Controller

```sh
➜  operator git:(main) ✗ make run
/Users/yumaojun/Projects/go-course/go13/operator/bin/controller-gen-v0.14.0 rbac:roleName=manager-role crd webhook paths="./..." output:crd:artifacts:config=config/crd/bases
/Users/yumaojun/Projects/go-course/go13/operator/bin/controller-gen-v0.14.0 object:headerFile="hack/boilerplate.go.txt" paths="./..."
go fmt ./...
go vet ./...
go run ./cmd/main.go
2024-05-05T12:04:16+08:00       INFO    setup   starting manager
2024-05-05T12:04:16+08:00       INFO    starting server {"kind": "health probe", "addr": "[::]:8081"}
2024-05-05T12:04:16+08:00       INFO    controller-runtime.metrics      Starting metrics server
2024-05-05T12:04:16+08:00       INFO    Starting EventSource    {"controller": "dnsrecord", "controllerGroup": "devops.go13.org", "controllerKind": "DnsRecord", "source": "kind source: *v1beta1.DnsRecord"}
2024-05-05T12:04:16+08:00       INFO    Starting Controller     {"controller": "dnsrecord", "controllerGroup": "devops.go13.org", "controllerKind": "DnsRecord"}
2024-05-05T12:04:16+08:00       INFO    controller-runtime.metrics      Serving metrics server  {"bindAddress": ":8080", "secure": false}
2024-05-05T12:04:16+08:00       INFO    Starting workers        {"controller": "dnsrecord", "controllerGroup": "devops.go13.org", "controllerKind": "DnsRecord", "worker count": 1}
```

```sh
➜  operator git:(main) ✗ kubectl apply -f config/samples/devops_v1beta1_dnsrecord.yaml 
dnsrecord.devops.go13.org/dnsrecord-sample created
```

```sh
2024-05-05T12:06:14+08:00       INFO    获取对象成功    {"controller": "dnsrecord", "controllerGroup": "devops.go13.org", "controllerKind": "DnsRecord", "DnsRecord": {"name":"dnsrecord-sample","namespace":"default"}, "namespace": "default", "name": "dnsrecord-sample", "reconcileID": "4d3a6e91-471a-4032-b4f3-8e692844e290", "service": "cmdb", "domain": "api.comdb.go13.com"}
2024-05-05T12:06:14+08:00       INFO    调用DNS来进行解析       {"controller": "dnsrecord", "controllerGroup": "devops.go13.org", "controllerKind": "DnsRecord", "DnsRecord": {"name":"dnsrecord-sample","namespace":"default"}, "namespace": "default", "name": "dnsrecord-sample", "reconcileID": "4d3a6e91-471a-4032-b4f3-8e692844e290"}
2024-05-05T12:06:14+08:00       INFO    调用DNS来进行解析成功   {"controller": "dnsrecord", "controllerGroup": "devops.go13.org", "controllerKind": "DnsRecord", "DnsRecord": {"name":"dnsrecord-sample","namespace":"default"}, "namespace": "default", "name": "dnsrecord-sample", "reconcileID": "4d3a6e91-471a-4032-b4f3-8e692844e290"}
```

```sh
➜  operator git:(main) ✗ kubectl delete DnsRecord dnsrecord-sample                 
dnsrecord.devops.go13.org "dnsrecord-sample" deleted
```

```sh
2024-05-05T12:09:50+08:00       INFO    执行删除逻辑    {"controller": "dnsrecord", "controllerGroup": "devops.go13.org", "controllerKind": "DnsRecord", "DnsRecord": {"name":"dnsrecord-sample","namespace":"default"}, "namespace": "default", "name": "dnsrecord-sample", "reconcileID": "ff8b722e-c07f-407f-9635-5862471a3421", "module": "dnsrecord"}
```