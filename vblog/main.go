package main

import (
	"gitlab.com/go-course-project/go13/vblog/cmd"

	// 通过import方法 完成注册
	_ "gitlab.com/go-course-project/go13/vblog/apps"
)

func main() {
	cmd.Execute()
}
