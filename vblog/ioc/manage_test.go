package ioc_test

import (
	"testing"

	"gitlab.com/go-course-project/go13/vblog/apps/token"
	"gitlab.com/go-course-project/go13/vblog/ioc"
)

func TestManageGetAndRegistry(t *testing.T) {
	ioc.Controller().Registry(token.AppName, &TestStruct{})
	t.Log(ioc.Controller().Get(token.AppName))

	// 对象内部 自己去ioc获取依赖
	// i.tokenSvc = ioc.Controller().Get(token.AppName)

	// 断言使用
	ioc.Controller().Get(token.AppName).(*TestStruct).XXX()

	// ioc管理
	// ioc.Init()
	// ioc.Destory()
}
