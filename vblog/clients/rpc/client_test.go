package rpc_test

import (
	"context"
	"testing"

	"gitlab.com/go-course-project/go13/vblog/apps/comment"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

func TestCreateComment(t *testing.T) {
	// 需要先建立网络链接
	WithInsecure := grpc.WithTransportCredentials(insecure.NewCredentials())
	conn, err := grpc.Dial("localhost:1234", WithInsecure)
	if err != nil {
		panic(err)
	}
	client := comment.NewServiceClient(conn)
	resp, err := client.CreateComment(context.Background(), &comment.CreateCommnetRequest{
		Content: "test1",
	})
	if err != nil {
		t.Fatal(err)
	}
	t.Log(resp)
}
