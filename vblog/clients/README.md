# Restful API Client


##  简单写法

```go
http.CLient{}.Post('url', {data})
```

有服务端配套出客户端(etcd server/etcd cli/etc client)

+ ali yun sdk + (restful 接口)
+ tencent sdk
+ ...

```js
export const LIST_BLOG = (params) =>
  client({
    url: '/vblog/api/v1/blogs/',
    method: 'get',
    params: params
  })

export const GET_BLOG = (id, params) =>
  client({
    url: `/vblog/api/v1/blogs/${id}`,
    method: 'get',
    params: params
  })

export const CRATE_BLOG = (data) =>
  client({
    url: '/vblog/api/v1/blogs/',
    method: 'post',
    data: data
  })

export const UPDATE_BLOG = (id, data) =>
  client({
    url: `/vblog/api/v1/blogs/${id}`,
    method: 'patch',
    data: data
  })

export const DELETE_BLOG = (id) =>
  client({
    url: `/vblog/api/v1/blogs/${id}`,
    method: 'delete',
  })
```


```go
func ListBlog(ctx context.Context, in *blog.QueryBlogRequest) (*blog.BlogSet, error) {
	return nil, nil
}
```