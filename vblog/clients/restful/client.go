package restful

import (
	"context"
	"fmt"

	"github.com/infraboard/mcube/v2/client/rest"
	"gitlab.com/go-course-project/go13/vblog/apps/blog"
	"gitlab.com/go-course-project/go13/vblog/apps/token"
)

// 服务接口(Service Interface)
func ListBlog(ctx context.Context, in *blog.QueryBlogRequest) (*blog.BlogSet, error) {
	// Network (http)
	// JSON

	// req := http.NewRequest("GET", "http://localhost:8010/vblog/api/v1/blogs/", OJB)
	// resp := http.DefaultClient.Do(req)

	// io.ReadAll(resp)
	// json.Unmarshal()
	// return resp
	return nil, nil
}

func NewClient(server, username, password string) (*Client, error) {
	client := &Client{
		c:  rest.NewRESTClient().SetBaseURL(server),
		tk: &token.Token{},
	}

	if err := client.login(username, password); err != nil {
		return nil, err
	}

	return client, nil
}

type Client struct {
	c  *rest.RESTClient
	tk *token.Token
}

// 调用 /login
func (c *Client) login(username, password string) error {
	err := c.c.
		Post("/vblog/api/v1/tokens").
		Body(token.NewIssueTokenRequest(username, password)).
		Do(context.Background()).
		Into(c.tk)
	if err != nil {
		return err
	}
	// 设置Header Token
	c.c.SetBearerTokenAuth(c.tk.AccessToken)
	return nil
}

// 服务接口(Service Interface)
func (c *Client) QueryBlog(ctx context.Context, in *blog.QueryBlogRequest) (*blog.BlogSet, error) {
	resp := blog.NewBlogSet()
	err := c.c.
		Get("/vblog/api/v1/blogs").
		Param("page_size", fmt.Sprintf("%d", in.PageSize)).
		Param("page_number", fmt.Sprintf("%d", in.PageNumber)).
		Param("keywords", in.Keywords).
		Do(ctx).
		Into(resp)
	return resp, err
}
