package restful_test

import (
	"context"
	"testing"

	// 注入Logger依赖
	"github.com/infraboard/mcube/v2/ioc"
	_ "github.com/infraboard/mcube/v2/ioc/config/log"

	"gitlab.com/go-course-project/go13/vblog/apps/blog"
	"gitlab.com/go-course-project/go13/vblog/clients/restful"
)

/*
client = New(api_server, username, password)
client.QueryBlog() ----> Restful API
client.CreateBlog() ----> Restful API
*/

// Restful SDK Client 测试
var (
	ctx    = context.Background()
	client *restful.Client
)

func TestQueryBlog(t *testing.T) {
	req := blog.NewQueryBlogRequest()
	resp, err := client.QueryBlog(ctx, req)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(resp)
}

func init() {
	// 先启动服务端, 再测试Restful Client
	ioc.DevelopmentSetup()

	c, err := restful.NewClient("http://127.0.0.1:8080", "admin", "123456")
	if err != nil {
		panic(err)
	}
	client = c
}
