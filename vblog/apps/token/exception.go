package token

import (
	"gitlab.com/go-course-project/go13/vblog/exception"
)

// 这个模块定义的业务异常
// token expired %f minitues
// 约定俗成:  ErrXXXXX 来进行自定义异常定义, 方便快速在包里搜索
var (
	ErrAccessTokenExpired  = exception.NewAPIException(5000, "access token expired")
	ErrRefreshTokenExpired = exception.NewAPIException(5001, "refresh token expired")
)
