package impl

import (
	"context"

	"gitlab.com/go-course-project/go13/vblog/apps/comment"
)

func (i *commentServiceImpl) CreateComment(
	ctx context.Context,
	in *comment.CreateCommnetRequest) (
	*comment.Comment, error) {
	return &comment.Comment{
		Spec: in,
	}, nil
}
